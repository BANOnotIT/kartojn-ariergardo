pub type Card = u8;
pub type UserId = u64;
pub type GameId = Vec<u8>;

//#[derive(Serialize, Deserialize, Debug)]
pub type EncryptedData = Vec<u8>;

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    id: UserId,
    nick: String,
    avatar: String,
}


#[derive(Serialize, Deserialize, Debug)]
pub struct Game {
    salt: u64,
    id: GameId,
    flags: Vec<u8>,
    // pack of cards
    deck: Vec<Card>,
    bench: Bench,
    instigator: User,
}


#[derive(Serialize, Deserialize, Debug)]
pub struct Bench {
    active: Vec<Vec<(UserId, Card)>>,
    dump: EncryptedData,
    players: Vec<UserState>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserState {
    id: UserId,
    salt: Vec<u8>,
    cards: EncryptedData,
    count: usize,
}


/*
impl EncryptedData {
    fn decrypt(&self, key: Vec<u8>) -> Vec<Card> {
        self.dump
    }

    fn encrypt(&self, cards: Vec<Card>, key: Vec<u8>) -> Vec<u8> {
        Vec::new()
    }

    fn shuffle(&self) {}
}
*/
